# BlockChain-crypto-currency

# Description
Basic working model of modern day crypto currencies, due to the time and resource constraints , low level hasing is used via object-hash npm package.

# INSTALLATION

STEP 1 : NPM install.

STEP 2 : Make post req on http://localhost:1337/Chain/start to start the process.

--------------------------------------------------------------------------
# SAMPLE OUTPUT  

Hashing:  e257acb08a5e885a3c088cae35a539309eb26fa3

Hashing:  06283c1602dc55c00b6db3318980bdf0aa76b136

Hashing:  4028d356d23f65f80042d784ae7c14c426a40eb0

Hashing:  7f2019551dea6ec9f96011ac2a3b5839b1d9a1a8

Hashing:  d141e1cfc6d8664a973cf98e2376e744437be1f6

Hashing:  61d1b79a486d328e030268151ba0d38d388075a6

Hashing:  895e27758f1ac37ef36a502eb19dd2e01490ac3f

Hashing:  027aa9c5f494e8c4801a3d67f3d056d93438f724

_verbo: block {
  index: 7,
  timestamp: 1641669327135,
  transactions: [ { sender: 'gokul', recipient: 'alex', amount: 200 } ],
  prevHash: '77fd4a89687362a07aca9002b06f98ef1c873b1c',
  hash: '6b7a5300c8ef22d9b988961ccba2cfcbbafb24e0'
}_


--------------------------------------------------------------------------




