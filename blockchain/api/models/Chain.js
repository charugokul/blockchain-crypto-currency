/**
 * Chain.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  // connection: 'mongoDB',
  attributes: {

    index: {
      required: true,
      type: 'number'
    },
    timestamp: {
      required: true,
      type: 'string'
    },
    transactions: {
      required: true,
      type: 'json'
    },
    prevHash: {
      required: false,
      type: 'string'
    },
    hash: {
      required: true,
      type: 'string'
    }

  }
};


