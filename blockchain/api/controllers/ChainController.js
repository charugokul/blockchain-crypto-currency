/**
 * ChainController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
 var chainService = require("../Services/chainService")
module.exports = {

start : async (req,res)=>{
    try {
        let block = await chainService.start() 
        res.send(block) 

    } catch (error) {
        res.badRequest(error)
    }
}
};

